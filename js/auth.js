/*
Copyright 2015 Google Inc. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


(function ($) {

    Drupal.behaviors.exampleModule = {
        attach: function (context, settings) {

            // The client ID is obtained from the Google Developers Console
            // at https://console.developers.google.com/.
            // If you run this code from a server other than http://localhost,
            // you need to register your own client ID.
            var OAUTH2_CLIENT_ID = drupalSettings.youtube_upload_widget.clientId;
            var OAUTH2_SCOPES = [
                'https://www.googleapis.com/auth/youtube'
            ];

            // Upon loading, the Google APIs JS client automatically invokes this callback.
            googleApiClientReady = function() {
                gapi.auth.init(function() {
                    window.setTimeout(checkAuth, 1);
                });
            }

            // Attempt the immediate OAuth 2.0 client flow as soon as the page loads.
            // If the currently logged-in Google Account has previously authorized
            // the client specified as the OAUTH2_CLIENT_ID, then the authorization
            // succeeds with no user intervention. Otherwise, it fails and the
            // user interface that prompts for authorization needs to display.
            function checkAuth() {
                gapi.auth.authorize({
                    client_id: OAUTH2_CLIENT_ID,
                    scope: OAUTH2_SCOPES,
                    immediate: true
                }, handleAuthResult);
            }

            // Handle the result of a gapi.auth.authorize() call.
            function handleAuthResult(authResult) {
                if (authResult && !authResult.error) {
                    // Authorization was successful. Hide authorization prompts and show
                    // content that should be visible after authorization succeeds.
                    $('.pre-auth').hide();
                    $('.post-auth').show();
                    loadAPIClientInterfaces();
                } else {
                    // Make the #login-link clickable. Attempt a non-immediate OAuth 2.0
                    // client flow. The current function is called when that flow completes.
                    $('#login-link').click(function() {
                        gapi.auth.authorize({
                            client_id: OAUTH2_CLIENT_ID,
                            scope: OAUTH2_SCOPES,
                            immediate: false
                        }, handleAuthResult);
                    });
                }
            }

            // Load the client interfaces for the YouTube Analytics and Data APIs, which
            // are required to use the Google APIs JS client. More info is available at
            // http://code.google.com/p/google-api-javascript-client/wiki/GettingStarted#Loading_the_Client
            function loadAPIClientInterfaces() {
                gapi.client.load('youtube', 'v3', function() {
                    handleAPILoaded();
                });
            }

        }
    };



}(jQuery));