<?php

namespace Drupal\youtube_widget_uploader\Plugin\Field\FieldWidget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\youtube\Plugin\Field\FieldWidget\YouTubeDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "field_youtube_widget",
 *   module = "youtube_widget_uploader",
 *   label = @Translation("Upload a video directly to Youtube."),
 *   field_types = {
 *     "youtube"
 *   }
 * )
 */
class YoutubeUploadWidget extends YouTubeDefaultWidget implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * YoutubeUploadWidget constructor.
   *
   * @param string $pluginId
   * @param mixed $pluginDefinition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   * @param array $settings
   * @param array $thirdPartySettings
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(
    $pluginId,
    $pluginDefinition,
    FieldDefinitionInterface $fieldDefinition,
    array $settings,
    array $thirdPartySettings,
    ConfigFactoryInterface $configFactory
  ) {
    parent::__construct($pluginId, $pluginDefinition, $fieldDefinition, $settings, $thirdPartySettings);
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $pluginId,
      $pluginDefinition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * @SuppressWarnings(PHPMD.CamelCaseParameterName)
   * @SuppressWarnings(PHPMD.CamelCaseVariableName)
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = $this->configFactory->get('youtube_widget_uploader.settings');
    $clientId = $config->get('client_id');
    $clientSecret = $config->get('client_secret');

    $widget = parent::formElement($items, $delta, $element, $form, $form_state);

    $widget['send_video_yt_group']['upload_video'] = [
      '#theme' => 'custom_form_upload_youtube',
      '#attached' => [
        'library' => [
          'youtube_widget_uploader/upload-video',
        ],
        'drupalSettings' => [
          'youtube_upload_widget' => [
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
          ],
        ],
      ],
    ];

    return $widget;
  }

}
