<?php

namespace Drupal\youtube_widget_uploader\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Exception;

/**
 * Class YouTubeConfigForm.
 */
class YouTubeConfigForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_credentials_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'youtube_widget_uploader.settings',
    ];
  }

  /**
   * @SuppressWarnings(PHPMD.CamelCaseParameterName)
   * @SuppressWarnings(PHPMD.CamelCaseVariableName)
   *
   * @throws \InvalidArgumentException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('youtube_widget_uploader.settings');

    $link_youtube_console = Link::fromTextAndUrl(t('Google Developers Console'),
      Url::fromUri('https://console.developers.google.com', [
        'attributes' => ['target' => '_blank'],
      ]))->toString();

    $form['text'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('You need to get the credentials on') . '<strong> ' . $link_youtube_console . '</strong></p>',
    ];
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Insert client ID from YouTube API.'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('Insert the secret key of Youtube API.'),
      '#default_value' => $config->get('client_secret'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   */
  public function submitForm(array &$form, FormStateInterface $formState) {

    // Saving data of form.
    $config = $this->configFactory()->getEditable('youtube_widget_uploader.settings');
    $config->set('client_id', $formState->getValue('client_id'));
    $config->set('client_secret', $formState->getValue('client_secret'));

    // Display result.
    try {
      $config->save();
      drupal_set_message(t('Credentials saved!'));
    }
    catch (Exception $e) {
      drupal_set_message(t('Error while saving the credentials.'));
    }

  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('administer youtube credentials'));
  }

}
